'use strict';

var mongoose = require('mongoose')
  , Grid = require('gridfs-stream')
  , fs = require('fs-extra')
  , path = require('path')
  , async = require('async');

var gfs = new Grid(mongoose.connection.db, mongoose.mongo);

module.exports.writeFile = function (opts, _file, cb) {
  var root = path.join(opts.dump, 'gridfs');
  var file = Object.assign({}, _file);
  var src = file.src;
  delete file.src;

  file.root = file.root || opts.rootCol || 'fs';

  var writeStream = gfs.createWriteStream(file);

  var fileSteam = fs.createReadStream(path.join(root, src));
  fileSteam.pipe(writeStream);
  writeStream.on('error', cb);
  writeStream.on('close', function (file) {
    cb(null, file);
  });
};

module.exports.clean = function (opts, cb) {
  var queries = [];

  var rootCol = opts.rootCol || 'fs';

  queries.push(function (cb) {
    mongoose.connection.db.dropCollection(rootCol + '.files', function () {
      cb();
    });
  });
  queries.push(function (cb) {
    mongoose.connection.db.dropCollection(rootCol + '.chunks', function () {
      cb();
    });
  });

  async.parallel(queries, cb);
};
