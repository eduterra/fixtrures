'use strict';

var path = require('path')
  , models = require('./models');

var Fixture = module.exports = function (config) {
  this.root = config.root;
  this.modelRoot = config.modelRoot;
  this.storageRoot = config.storageRoot;
  this.models = models(this.modelRoot);
  this.gridFS = config.gridFS;
};

Fixture.prototype.restore = function (id, clean, done) {
  var fixture = this;
  if (typeof clean == 'function') {
    done = clean;
    clean = true;
  }

  require('./restore')(fixture, {
    dump: path.join(this.root, id),
    clean: clean,
    gridFS: this.gridFS
  }, done);

};
