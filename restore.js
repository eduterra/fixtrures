'use strict';

var async = require('async')
  , fs = require('fs-extra')
  , path = require('path')
  , debug = require('debug')('fixture:debug')
  , gridFS = require('./gridfs');

module.exports = function (fixture, opts, done) {
  var dumpDir = opts.dump;
  var queries = [];
  // 1. Cleanup previous data, if asked to
  if (opts.clean) {
    queries.push(function (cb) {
      async.eachSeries(fixture.models, function (Model, cb) {
        Model.remove({}, cb);
      }, cb);
    });
  }
  // 2. Cleanup GridFS
  if (opts.clean && opts.gridFS) {
    queries.push(function (cb) {
      gridFS.clean(opts, cb);
    });
  }
  // 3. Copy storage directory, if it exists
  queries.push(function (cb) {
    var src = path.join(dumpDir, 'storage');
    fs.stat(src, function (ignoredErr, stat) {
      if (!stat || !stat.isDirectory())
        return cb();
      debug('cp -r %s %s', src, fixture.storageRoot);
      fs.copy(src, fixture.storageRoot, cb);
    });
  });
  // 4. Read and persist models
  fixture.models.forEach(function (Model) {
    queries.push(function (cb) {
      var file = path.join(dumpDir, Model.modelName + '.json');
      fs.readJson(file, 'utf-8', function (ignoredErr, objs) {
        if (!objs) return cb(); // Ignore missing files
        debug('Restoring from %s', file);
        if (Array.isArray(objs))
          async.each(objs, function (obj, cb) {
            new Model(obj).save(cb);
          }, cb);
        else
          new Model(objs).save(cb);
      });
    });
  });
  // 5. Persist files for GridFS
  if (opts.gridFS) {
    queries.push(function (cb) {
      var root = path.join(dumpDir, 'gridfs');
      var filesJSON = path.join(root, 'files.json');
      fs.readJson(filesJSON, 'utf-8', function (ignoredErr, files) {
        if (!files) return cb();

        debug('Restoring files to GridFS');

        async.eachSeries(files, function (file, cb) {
          gridFS.writeFile(opts, file, cb);
        }, cb);
      });
    });
  }
  // All done
  async.series(queries, done);
};

